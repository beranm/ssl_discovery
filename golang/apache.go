package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os/exec"
	"regexp"
	"strings"
)

func checkVhosts(outputApachectl string) []ServerData {
	rgx := regexp.MustCompile(`port ([a-zA-Z0-9]+) namevhost ([_0-9a-zA-Z\-\.\*]+).+alias.([_0-9a-zA-Z\-\.\*]+)`)
	matches := rgx.FindAllStringSubmatch(outputApachectl, -1)

	dataArray := make([]ServerData, 0, len(matches))
	for _, match := range matches {
		// domain without alias
		if len(match) > 2 {
			data := ServerData{
				ServerName: match[2],
				ServerPort: match[1],
			}
			if match[1] == "80" {
				data.ServerProto = "http"
				data.ServerIgnore = checkIgnore(match[2], "http")
			} else {
				data.ServerProto = "https"
				data.ServerIgnore = checkIgnore(match[2], "https")
			}
			if !ServerDataContains(dataArray, data) {
				dataArray = append(dataArray, data)
			}
		}

		// domain with alias
		if len(match) > 3 {
			data := ServerData{
				ServerName: match[3],
				ServerPort: match[1],
			}
			if match[1] == "80" {
				data.ServerProto = "http"
				data.ServerIgnore = checkIgnore(match[2], "http")
			} else {
				data.ServerProto = "https"
				data.ServerIgnore = checkIgnore(match[2], "https")
			}
			if !ServerDataContains(dataArray, data) {
				dataArray = append(dataArray, data)
			}
		}

	}
	return dataArray
}

func formatApacheOutput(outputApachectl string) string {

	scanner := bufio.NewScanner(strings.NewReader(outputApachectl))

	var prevLine string
	var output strings.Builder

	for scanner.Scan() {
		line := scanner.Text()

		if strings.Contains(line, "alias") {
			prevLine = strings.TrimSpace(prevLine)
			line = strings.TrimSpace(line)
			re := regexp.MustCompile(`[ \t]+`)
			prevLine = re.ReplaceAllString(prevLine, " ")
			line = re.ReplaceAllString(line, " ")
			output.WriteString(prevLine + " " + line + "\n")
			continue
		}

		output.WriteString(line + "\n")
		prevLine = line
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Cannot read config string:", err)
	}

	return output.String()
}

func checkDefaults(outputApachectl string) []ServerData {
	rgx := regexp.MustCompile(`\*:([0-9]+)\s*([_0-9a-zA-Z\-\.\*]+)\s\(.+:[0-9]*\)`)
	matches := rgx.FindAllStringSubmatch(outputApachectl, -1)

	dataArray := make([]ServerData, 0, len(matches))
	for _, match := range matches {
		if len(match) > 2 {
			data := ServerData{
				ServerName: match[2],
				ServerPort: match[1],
			}
			if match[1] == "80" {
				data.ServerProto = "http"
				data.ServerIgnore = checkIgnore(match[2], "http")
			} else {
				data.ServerProto = "https"
				data.ServerIgnore = checkIgnore(match[2], "https")
			}
			dataArray = append(dataArray, data)
		}
	}
	return dataArray
}

func checkApache(binName string) ([]ServerData, error) {
	cmd := exec.Command(binName, "-S")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return nil, err
	}

	outputApachectl := out.String()
	vhosts := checkVhosts(formatApacheOutput(outputApachectl))
	defaultsHosts := checkDefaults(outputApachectl)

	return append(vhosts, defaultsHosts...), nil
}
